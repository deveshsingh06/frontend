import { Location } from "./Location.model";

export interface College {
  collegeId?: number;
  collegeName: string;
  collegeWebsite: string;
  collegeNirfRanking: number;
  collegeAicteAffiliation: boolean;
  collegeLocation: Location;
  collegeEmail: string;
}