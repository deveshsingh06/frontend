import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyService } from 'src/app/services/company/company.service';
import { UserService } from 'src/app/services/user/user.service';
import { Company } from 'src/app/shared/models/Company.model';

@Component({
  selector: 'app-search-company',
  templateUrl: './search-company.component.html',
  styleUrls: ['./search-company.component.css'],
})
export class SearchCompanyComponent implements OnInit {
  // List of all companies | Dummy object for testing
  companies: Company[] = [];

  constructor(
    private companyService: CompanyService,
    private userService: UserService,private router:Router
  ) {}

  ngOnInit(): void {
    this.companyService.getAllCompanies().subscribe({
      next: (result) => {
        this.companies = result;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => console.info('complete')
    })
  }

  userRequestCompany(company: Company) {
    this.companyService.updateUserCompany(company).subscribe({
      next: (result) => {
        this.router.navigate(['login']);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => console.info('complete')
    })
  }
}
