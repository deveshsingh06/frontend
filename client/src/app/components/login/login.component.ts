import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { UserService } from 'src/app/services/user/user.service';
import { DismissiveAlertComponent } from '../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private userService: UserService,
    private router: Router,
    private authService: AuthenticationService
  ) {}

  credentials: any = {
    email: '',
    password: '',
  };

  //Alert Component whose add method is gonna be accesed here
  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  ngOnInit(): void {}

  onLogin() {
    this.authService
      .authenticate(this.credentials.email, this.credentials.password)
      .subscribe({
        next: (res: any) => {
          this.alert.add('success', 'Loggedin Successfully', 5000);
          //redirection

          this.userService.setLoggedInUser(res);
          //redirect to home page
          //redirection
          //redirect to login page
          this.router.navigate(['home']);
        },
        error: (err: any) => {
          console.log(err);
          this.alert.add('danger', 'Please enter correct credentials', 3000);
        },
        complete: () => console.info('complete'),
      });
  }
}
