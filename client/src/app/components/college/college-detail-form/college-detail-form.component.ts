import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { College } from 'src/app/shared/models/College.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-college-detail-form',
  templateUrl: './college-detail-form.component.html',
  styleUrls: ['./college-detail-form.component.css'],
})
export class CollegeDetailFormComponent implements OnInit {
  @Input()
  college: College;

  @Input()
  buttonName!: string;

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  collegeFormSubmit(): void {
    this.formSubmit.emit(this.college);
  }
}
